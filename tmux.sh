#!/bin/bash
session=wsiPipeline
login=${session}:0
scs5=${session}:1
ml=${session}:2
biomag=${session}:3
ssd=${session}:4
#biomagquick=${session}:4
#tmux kill-session -t "$session"
tmux new-session -d -s "$session" -n "login-base-venv (for sbatch+other)"
tmux new-window -n "scs5-wsi-venv (for vscode pkg install)" -t "$scs5"
tmux new-window -n "ml-wsi-venv (for ml-jnb pkg install)" -t "$ml"
tmux new-window -n "gpu2-biomagdsb (for biomagdsb scripts+bg-jnb)" -t "$biomag"
tmux new-window -n "ssd" -t "ssd"
#tmux new-window -n "gpu2-biomagddsb-quick (Until windows 3 is available)" -t "$biomagquick"
tmux send-keys -t "$login" C-z "cd ~/" Enter
tmux send-keys -t "$scs5" C-z "conda activate x64-wsi-pipeline" Enter
tmux send-keys -t "$scs5" "cd ~/workspaces/scratch/s7740678-wsi_pipeline_data/ml-based-image-classification-in-blast-pathology/" Enter
tmux send-keys -t "$ml" C-z "srun --pty -p ml -n 1 --gres=gpu:2 -c 16 --mem=64G -w taurusml12 -t 06:00:00 bash -l -i" Enter
tmux send-keys -t "$ml" "conda activate jnb-tf" Enter
tmux send-keys -t "$ml" "jupyter lab --config=/home/h7/s7740678/.jupyter/jupyterhub_config.py > ~/jupyter/taurusml12-jnb.log 2>&1 &" Enter
tmux send-keys -t "$ml" "conda activate /home/s7740678/user-kernel/ml-wsi-pipeline" Enter
tmux send-keys -t "$ml" "cd ~/workspaces/scratch/s7740678-wsi_pipeline_data/ml-based-image-classification-in-blast-pathology/" Enter
tmux send-keys -t "$biomag" C-z "srun --pty -p gpu2-interactive -n 1 --gres=gpu:0 -c 2 --mem=32G -t 08:00:00 -w taurusi2107 bash -l -i" Enter
tmux send-keys -t "$biomag" "module load TensorFlow/1.10.0-fosscuda-2018b-Python-3.6.6" Enter
tmux send-keys -t "$biomag" "conda activate gpu2-jnb" Enter
tmux send-keys -t "$biomag" "jupyter lab --config=/home/h7/s7740678/.jupyter/jupyterhub_config.py > ~/jupyter/taurusi2107-jnb.log 2>&1 & " Enter
#tmux send-keys -t "$biomag" "module load TensorFlow/1.10.0-fosscuda-2018b-Python-3.6.6" Enter
#tmux send-keys -t "$biomag" "module load MATLAB" Enter
tmux send-keys -t "$biomag" "cd ~/workspaces/scratch/s7740678-wsi_pipeline_data/ml-based-image-classification-in-blast-pathology/wsi_pipeline/libs/biomagdsb_cell_segmentation/" Enter
tmux send-keys -t "$biomag" "conda activate x64-wsi-pipeline" Enter
tmux send-keys -t "$ssd" C-z "cd ~/workspaces/ssd/s7740678-wsi_pipeline_tmp/" Enter
#tmux send-keys -t "$biomagquick" C-z "srun --pty -p gpu2-interactive -n 1 --gres=gpu:1 -c 2 --mem=16G -w taurusi2089 -t 01:00:00 bash -l -i" Enter
#tmux send-keys -t "$biomagquick" "cd ~/workspaces/scratch/s7740678-wsi_pipeline_data/ml-based-image-classification-in-blast-pathology/wsi_pipeline/libs/biomagdsb_cell_segmentation/" Enter
#tmux send-keys -t "$biomag" "conda activate ./venv" Enter
tmux attach -t "$session"

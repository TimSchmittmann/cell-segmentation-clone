import functools

def iset(iterable):
    for i, x in enumerate(iterable):
        def setter(value):
            iterable[i] = value
        yield setter, x


def catch(func):
    @functools.wraps(func)
    def wrapper_catch(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            print(e)
    return wrapper_catch

def batchify(iterable, batch_size=1):
    batch = []
    for el in iterable:
        batch.append(el)
        if len(batch) == batch_size:
            yield batch
            batch = []
import os

import functools

def chcwd(func, new_cwd):
    @functools.wraps(func)
    def wrapper_chcwd(*args, **kwargs):
        cwd = os.getcwd()
        os.chdir(new_cwd)
        f = func(*args, **kwargs)
        os.chdir(cwd)
        return f 
    return wrapper_chcwd

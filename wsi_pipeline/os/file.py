import os
import shutil
import wget
import zipfile
from pathlib import Path
from wsi_pipeline.core.data_loop_step import DataLoopStep
from wsi_pipeline.core.data import Data
from typing import List
#from wsi_pipeline.core.step import Step

#@DataLoopStep.make
def copy_file(file_path: Path, to_directory: Path, force_overwrite=False):
    print(f"Copy {file_path} to {to_directory}")
    target_path =  to_directory / file_path.stem
    if target_path.isfile() and not force_overwrite:
        return
    shutil.copy(file_path, target_path)

#@DataLoopStep.make
def move_file(file_path: Path, to_directory: Path, force_overwrite=False):
    target_path = to_directory / file_path
    if target_path.isfile() and not force_overwrite:
        return 
    shutil.move(file_path, target_path) 

#@DataLoopStep.make
def glob_paths(from_dir: Path, pattern="*") -> List[str]:
    print(f"Glob {from_dir}")
    return from_dir.glob(pattern)
"""
@as_step
def copy_files(self, from_directory, to_directory, pattern="*", force_overwrite=False, **kwargs):
    for path in Path(from_directory).glob(pattern):
        target_path = os.path.join(to_directory, os.path.basename(path))
        if os.path.isfile(target_path) and not force_overwrite:
            continue
        shutil.copy(path, to_directory) 
"""
#@DataLoopStep.make
def extract_zip(zip_file: Path, extraction_target_dir, force_overwrite=False):
    print(f"Unzip {zip_file}")
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(extraction_target_dir)

"""
@as_step
def download_and_extract_zip(tmp_file = "tmp.zip", **kwargs):
    if os.listdir(extraction_target_dir) != [] and not force_overwrite:
        return 
    if not os.path.isdir(extraction_target_dir):
        os.mkdir(extraction_target_dir)
    wget.download(url, tmp_file)

    pathlib.Path.unlink(tmp_file)
"""
#@DataLoopStep.make
def mkdir_if_not_exist(path: Path):
    if not path.isdir():
        path.mkdir()
        
#@DataLoopStep.make
def download_file(url: str, file: Path, overwrite_existing=False):
    print(f"Download {url}")
    if not file.is_file():
        wget.download(url, str(file))

#@DataLoopStep.make
def delete_file(file_path: Path) -> None:
    print(f"Delete {file_path}")
    if file_path.is_file():
        file_path.unlink()


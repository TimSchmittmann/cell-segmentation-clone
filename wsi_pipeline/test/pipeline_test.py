from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step
from wsi_pipeline.core.data_step import DataStep
from wsi_pipeline.core.data import Data
from wsi_pipeline.util.functions import iset
import pathlib
import wget
import os
import zipfile


class TestSteps(Pipeline):

    def __init__(self):
        self.test = "xyz"

    @Step.make
    def nothing_0(self, url):
        print("Do nothing 0")

    @Step.make(name="function_nothing_1")
    def nothing_1(self, url):
        print("Do nothing 1")

    @DataStep.make
    def append_0(self, url, data: Data = Data()):
        print("Append 0")
        print(data)
        data.append(0)
        return data

    @DataStep.make(name="function_increase_1")
    def increase_1(self, url, data: Data = Data()):
        print("Increase all items by 1")
        for idx in range(len(data)):
            data[idx]+=1
        return data

    @DataStep.make
    def append_2(self, url, data: Data = Data()):
        print("Append 2")
        data.append(2)
        return data

    @DataStep.make(name="function_increase_2")
    def increase_2(self, url, data: Data = Data()):
        print("Increase all items by 2")
        for idx in range(len(data)):
            data[idx]+=1
        return data
    
    @DataStep.make(name="function_increase_n")
    def increase_n(self, n, data: Data = Data()):
        print(f"Increase all items by {n}")
        for idx in range(len(data)):
            data[idx]+=n
        return data

if __name__ == "__main__":
    # execute only if run as a script
    t = TestSteps()
    p = Pipeline("Pipeline", remember_data=True)
    p.add(t.nothing_0("url_nothing_0"))
    p.add(t.nothing_1("url_nothing_1"))
    p.add(t.append_0("url_append_0"))
    p.add(t.increase_1("url_increase_1"))
    p.add(t.append_2("url_append_2"))
    p.add(t.increase_2("url_increase_2"))
    p.add(t.increase_n(10))
    p.add(t.increase_n(100))
    p.add(lambda: print(os.path.realpath(__file__)))
    print(p)
    print(p.steps)
    p()
    print(p.data_at_step)
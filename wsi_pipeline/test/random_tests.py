def demo_function(arg0, **kwargs):
    data = kwargs["data"]
    print(arg0, data)

def wrapper(arg0, data):
    demo_function(arg0, data=data)

if __name__ == "__main__":
    # execute only if run as a script
    wrapper(0, "demo_arg")
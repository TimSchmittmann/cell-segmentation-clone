from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step
from wsi_pipeline.core.data_step import DataStep
from wsi_pipeline.core.data import Data
import wsi_pipeline.os.file as File
from wsi_pipeline.util.functions import iset
from pathlib import Path
import wget
import zipfile

if __name__ == "__main__":
    # execute only if run as a script
    """
    p = Pipeline("Pipeline", remember_data=True)
    p.add(lambda: ["https://schmittmann.me/index.php/s/or393w0KtB8EQjV/Download", "https://schmittmann.me/index.php/s/7pixWLNCdxqiFPv/Download"])
    p.add(File.download_file("tmp.zip"))
    p.add(File.mkdir_if_not_exist("tmp.zip"))
    p.add(File.extract_zip("tmp_test"))
    """
    data = { "ds1": { "url":"https://schmittmann.me/index.php/s/or393w0KtB8EQjV/download"}, 
             "ds2": {"url": "https://schmittmann.me/index.php/s/7YOCYTHIROv4C9X/download"}}
    tmp_base_dir = Path("tmp")
    for key, ds in data.items():
        tmp_file = ds["tmp_file"] = Path(f"{key}_tmp.zip")
        File.download_file(ds["url"], tmp_file)
        tmp_dir = ds["tmp_dir"] = tmp_base_dir / tmp_file.stem
        tmp_dir.mkdir(parents=True, exist_ok=True)
        File.extract_zip(tmp_file, tmp_dir)
    print(data)

    """
    p.add(t.nothing_0("url_nothing_0"))
    p.add(t.nothing_1("url_nothing_1"))
    p.add(t.append_0("url_append_0"))
    p.add(t.increase_1("url_increase_1"))
    p.add(t.append_2("url_append_2"))
    p.add(t.increase_2("url_increase_2"))
    p.add(t.increase_n(10))
    p.add(t.increase_n(100))
    p.add(lambda: print(os.path.realpath(__file__)))
    """
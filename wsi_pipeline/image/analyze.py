import numpy as np

def get_avg_foreground_color(self, tile, cell_roi_masks_union):
    """Calculate average color of cell pixels in tile.
    
    Parameters
    ----------
    tile : ndarray
        The image to calculate foreground color from
    cell_roi_masks_union : ndarray
        Binary mask to identify cell pixels
        
    Returns
    -------
    foreground_color : ndarray
        Average rgb foreground color 
    """
    foreground = np.copy(tile)
    foreground = foreground[cell_roi_masks_union == 1]
    
    foreground_color = np.average(foreground, axis=(0))
    return foreground_color

def get_avg_background_color(self, tile, cell_roi_masks_union):
    """Calculate artificial average background color of non-cell pixels in tile.
    To better separate background and foreground, we artificially increase the difference 
    between foreground and background.
    
    Parameters
    ----------
    tile : ndarray
        The image to calculate background color from
    cell_roi_masks_union : ndarray
        Binary mask to identify non-cell pixels
        
    Returns
    -------
    background_color : ndarray
        Enhanced average rgb background color 
    """
    background = np.copy(tile)
    background = background[cell_roi_masks_union == 0]
    background_color = np.average(background, axis=(0))
    
    diff = background_color-self.get_avg_foreground_color(tile, cell_roi_masks_union)
    # Move background further away from foreground
    background_color += diff
    background_color = np.clip(background_color, 0,255)
    return background_color


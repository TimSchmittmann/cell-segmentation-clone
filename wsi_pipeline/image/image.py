from pathlib import Path

class ImageMutations(object):

    def __init__(self, name):
        self.name = name
        self.mutations = {}

    @property
    def mutations(self):
        return self.mutations

    def add(self, key, mutation):
        self.mutations[key] = mutation


class NamedImage(object):

    def __init__(self, name, np_array):
        self.np_array = np_array
        self.name = name

    def __str__(self):
        return f"NamedImage: {self.name}"

    #@property
    #def ndarray(self) -> np.ndarray:
    #    return self.__ndarray

    #@property
    #def id(self):
    #    return self.__id
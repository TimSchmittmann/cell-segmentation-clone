import numpy as np
import matplotlib.pyplot as plt
from wsi_pipeline.image.transform import crop_image_only_outside


'''
def save_segmented_cells(filename, magnification, results_type, segmented_cells):
    """Loop all segmented and extracted cell patches and save them inside 
    their specific results_type+magnification directory"""
    for i in range(len(segmented_cells)):
        cell = segmented_cells[i]
        results_dir = make_results_type_magnification_dir(results_type, magnification)
        
        plt.imsave(results_dir / filename+'_cell'+str(i)+'.png', cell)

def save_cell_masks(filename, magnification, results_type_dir, original_tile, cell_masks):
    """Loop all cell masks and extract the corresponding patches from the original tile. 
    Save the patches inside the specific results_type+magnification directory"""
    cells = []
    for i in range(cell_masks.shape[2]):
        cell_mask = np.copy(original_tile)
        cell_mask[cell_masks[:,:,i] == 0] = 0
        cells.append(crop_image_only_outside(cell_mask))
    save_segmented_cells(filename, magnification, results_type_dir, cells)

def make_results_type_magnification_dir(result_type, magnification):
        """Helper function to create result directories"""
        results_type_dir = results_dir / result_type
        if not results_type_dir.is_dir():
            results_type_dir.mkdir()
        results_type_magnification_dir = results_type_dir / str(int(magnification))
        if not results_type_magnification_dir.is_dir():
            results_type_magnification_dir.mkdir()
        return results_type_magnification_dir
    
def save_figure_results(results_type, magnification, filename):
    """Helper function to save figures and create necessary directories"""
    results_dir = make_results_type_magnification_dir(results_type, magnification)
    plt.savefig(results_dir / filename+'.png')


def rois_to_masks(rois, tile):
    """Transforms boundary boxes in format [xmin, ymin, xmax, ymax] into binary masks with the shape of tile"""
    roi_masks = []
    for roi in rois:
        roi = roi.astype(int)
        roi_mask = np.zeros((tile.shape[0], tile.shape[1]))
        roi_mask[roi[0]:roi[2], roi[1]:roi[3]] = 1
        roi_masks.append(roi_mask)
    return roi_masks

def get_extended_cell_rois(results, original_tile, rescaled_rois):
    """Extend cell rois by 1/4th of their size"""
    for roi in rescaled_rois:
        dx = int((roi[2] - roi[0]) / 4)
        dy = int((roi[3] - roi[1]) / 4)
        roi[0] -= dx
        roi[1] -= dy
        roi[2] += dx
        roi[3] += dy
    return rescaled_rois

def unify_cell_roi_masks(tile, cell_roi_masks):
    """Simple union of cell_roi_masks"""
    cell_roi_masks_union = np.zeros(tile.shape)
    for roi_mask in cell_roi_masks:
        cell_roi_masks_union[roi_mask == 1] = 1
    return cell_roi_masks_union

def get_segmented_cells_rois(tile, rescaled_masks, cell_roi_masks, cell_roi_masks_union, background_color):
    """Extract the segmented cells as rectangular extraction from the original tile based on their roi_masks.
    Also uses the rescaled_masks to remove neighbouring cells from the final extracted patches.
    
    Parameters
    ----------
    tile : ndarray
        The (original) tile to extract the rectangular cell images from
    rescaled_masks : ndarray
        Binary masks used to find neighbouring cells. Same shape as tile
    cell_roi_masks : ndarray
        The roi masks used to extract the rectangular cell images from tile
    cell_roi_masks_union : ndarray
        Union of all cell_roi_masks to quickly get all cell/non-cell pixels 
    background_color : ndarray
        The average background color of non-cell pixels of tile
    
    Returns
    -------
    cell_rois : ndarray
        Extracted the rectangular cell images from tile
    cell_rois_wo_neighbor : ndarray
        Extracted the rectangular cell images from tile with neighbouring cells replaced by background_color  
    """
    img_homogenous_background = np.copy(tile)
    img_homogenous_background[cell_roi_masks_union == 0] = background_color
    
    cell_rois_wo_neighbor = []
    cell_rois = []
    for i in range(len(cell_roi_masks)):
        mask = cell_roi_masks[i]
        if not mask.any(): 
            continue
        
        cell_roi = np.copy(img_homogenous_background)
        cell_roi[mask == 0] = 0
        cell_rois.append(crop_image_only_outside(cell_roi))
        
        cell_roi_wo_neighbor = np.copy(img_homogenous_background)
        
        for j in range(rescaled_masks.shape[2]):
            if i != j:
                cell_roi_wo_neighbor[rescaled_masks[:,:,j].astype('int') > 0] = background_color
            
        cell_roi_wo_neighbor[mask == 0] = 0
        cell_rois_wo_neighbor.append(crop_image_only_outside(cell_roi_wo_neighbor))
    return (cell_rois, cell_rois_wo_neighbor)
'''
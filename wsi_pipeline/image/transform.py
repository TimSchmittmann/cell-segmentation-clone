def crop_image_only_outside(self, img,tol=0):
    """Crop image to remove all pixels below threshold/tolerance on the borders
    of the image (https://codereview.stackexchange.com/questions/132914/crop-black-border-of-image-using-numpy)
    
    Parameters
    ----------
    img : ndarray
        Image to crop
    tol : int, optional
        Threshold. Crop any pixels on the borders of img below this. 
    
    Returns
    -------
    img : ndarray
        Cropped image
    """
    mask = img>tol
    if img.ndim==3:
        mask = mask.all(2)
    m,n = mask.shape
    mask0,mask1 = mask.any(0),mask.any(1)
    col_start,col_end = mask0.argmax(),n-mask0[::-1].argmax()
    row_start,row_end = mask1.argmax(),m-mask1[::-1].argmax()
    return img[row_start:row_end,col_start:col_end]


def img_to_tiles(img, target_tile_size):
    """Split image in evenly sized tiles. Crops any leftover pixel that are not large enough for another tile"""
    S = target_tile_size
    return [img[x:x+S,y:y+S] for x in range(0,int(img.shape[0] / S) * S,S) for y in range(0,int(img.shape[1] / S) * S,S)]
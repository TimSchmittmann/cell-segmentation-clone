from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step
from wsi_pipeline.experiments.biomagdsb_cell_segmentation.adapters import StyleTransferSegmentationAdapter
#from wsi_pipeline.experiments.shared.datasets import SharedDatasets, TMP_BASE_DIR
from pathlib import Path
import wsi_pipeline.os.file as File
from wsi_pipeline.image.id_image import IdImage
from typing import List
import wget
import zipfile
import os
import shutil
from itertools import chain
import skimage.io
import skimage.transform
from wsi_pipeline.util.functions import catch
from wsi_pipeline.loader.generators import FsImgReader
import pandas as pd
#from PIL import Image

ROOT_DIR = os.path.abspath("")
DATA_DIR = os.path.join(ROOT_DIR, 'data')
RESULTS_DIR = os.path.join(ROOT_DIR, 'results')
RUN_DIR = os.path.join(ROOT_DIR, 'run')
TMP_DIR = Path.home() / 'workspaces' / 'ssd' / 's7740678-wsi_pipeline_tmp'
WSI_DIR = TMP_DIR / 'data' / 'wsi_originals'



def prepare_images_ds1(data):
    ds1_img_iter = Path(data["ds1"]["tmp_dir"] / "60").glob("*.jpg")
    id_images = []
    for img_path in ds1_img_iter:
        im = skimage.io.imread(img_path)
        im = skimage.transform.resize(im, (im.shape[0] // 2, im.shape[1] // 2), anti_aliasing=True)
        id_images.append(IdImage(f"ds1_{img_path.name}", im))
    return id_images

def prepare_images_ds2(data):
    ds2_img_iter = chain(Path(data["ds2"]["tmp_dir"] / "m3").glob("*.tif"),
                        Path(data["ds2"]["tmp_dir"] / "other").glob("*.tif"))
    id_images = []
    for img_path in ds2_img_iter:
        im = skimage.io.imread(img_path)
        im = skimage.transform.resize(im, (im.shape[0] // 2, im.shape[1] // 2), anti_aliasing=True)
        id_images.append(IdImage(f"ds2_{img_path.name}", im))
    return id_images

def pipeline_experiment_1():
    adapter = StyleTransferSegmentationAdapter()
    df = pd.read_csv(WSI_DIR / 'combined_wsi.csv')


    print(df)
    #adapter = StyleTransferSegmentationAdapter()
    #datasets = SharedDatasets()
    #datasets.retrieve_images
    """
    unet_models_dir = TMP_BASE_DIR / "unet_models"
    mask_rcnn_model_dir = TMP_BASE_DIR / "mask_rcnn_models"
    retrieve_models(unet_models_dir, mask_rcnn_model_dir)
    adapter.setup_models(unet_models_dir / "unet", mask_rcnn_model_dir / "model")
    """

    #id_images = prepare_images_ds2(datasets.data)
    #adapter.prepare_images(id_images)



    """
    im = skimage.io.imread(image)
    im = skimage.transform.resize(im, (im.shape[0] // 2, im.shape[1] // 2), anti_aliasing=True)



    experiment_data_sub_dirs = ["fab_test_images"]
    url_loader = UrlLoader()
    fs_loader = FilesystemLoader()
    p = Pipeline("Pipeline", remember_data=False)
    p.add(Step.make(lambda _: os.chdir(adapters.base.root_dir)))
    # U-Net models
    p.add(url_loader.download_and_extract_zip("https://schmittmann.me/index.php/s/zvbs8KfBuyMf52H/download", adapters.base.unet_model_dir, False))
    # Mask RCNN models
    p.add(url_loader.download_and_extract_zip("https://schmittmann.me/index.php/s/JMxvRQG7HPO7xS5/download", adapters.base.mask_rcnn_model_dir, False))
    for data_sub_dir in experiment_data_sub_dirs:
        p.add(fs_loader.copy_files(data_sub_dir, adapters.base.orig_dir))
    p.add(adapters.start_preprocessing())
    p.add(adapters.prepare_prediction())
    p.add(adapters.start_prediction_fast())
    p.add(os_chdir(ROOT_DIR))
    p()
    """
if __name__ == "__main__":
    # execute only if run as a script
    pipeline_experiment_1()
    #pipeline = pipeline_experiment_1()

    #pipeline()

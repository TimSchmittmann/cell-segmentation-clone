from wsi_pipeline.libs.biomagdsb_cell_segmentation.style_transfer_segmentation import StyleTransferSegmentation
import wsi_pipeline.os.file as File
from wsi_pipeline.os.cwd import chcwd
from wsi_pipeline.image.image import NamedImage
from wsi_pipeline.util.functions import catch
import skimage.io
import skimage.transform
import skimage.util
from pathlib import Path
from typing import List
from typing import Iterable
import os
import shutil
import glob

class DefaultStyleTransferSegmentationAdapter(object):

    def __init__(self):
        self.base = StyleTransferSegmentation()
        self.base.create_directories()
        self.image_filename_mapping = {}

    def copy_models(self, unet_models_dir, mask_rcnn_models_dir):
        self.base.cleanup_models()
        shutil.copytree(unet_models_dir, self.base.unet_model_dir)
        shutil.copytree(mask_rcnn_models_dir, self.base.mask_rcnn_model_dir)

    def setup_models(self, tmp_dir, skip_download = False,
                    unet_models_url = "https://schmittmann.me/index.php/s/zvbs8KfBuyMf52H/download",
                    mask_rcnn_model_url = "https://schmittmann.me/index.php/s/JMxvRQG7HPO7xS5/download"):

        unet_models_tmp_dir = tmp_dir / "unet_models" / "unet"
        mask_rcnn_models_tmp_dir = tmp_dir / "mask_rcnn_models" / "model"

        catch(unet_models_tmp_dir.mkdir)(parents=True, exist_ok=True)
        catch(mask_rcnn_models_tmp_dir.mkdir)(parents=True, exist_ok=True)

        if not skip_download:
            File.download_file(unet_models_url,  Path(f"{unet_models_tmp_dir}.zip"))
            catch(File.extract_zip)(Path(f"{unet_models_tmp_dir}.zip"), unet_models_tmp_dir)

            File.download_file(mask_rcnn_model_url, Path(f"{mask_rcnn_models_tmp_dir}.zip"))
            catch(File.extract_zip)(Path(f"{mask_rcnn_models_tmp_dir}.zip"), mask_rcnn_models_tmp_dir)

        self.copy_models(unet_models_tmp_dir, mask_rcnn_models_tmp_dir)

    def load_interim_results(self, result_dirs):
        interim_results = {}
        for result_dir in result_dirs:
            for path in glob.glob(str(result_dir / "*")):
                id = Path(path).stem
                if id not in interim_results:
                    interim_results[id] = {}
                interim_results[id][result_dir.name] = NamedImage(Path(path).name, skimage.io.imread(path))
        #for path in glob.glob(str(self.base.pos / "*")):
        #    interim_results['postprocessing'].append(FsImage(path, skimage.io.imread(path)))
        return interim_results

    def save_interim_results(self, interim, target_dir, file_prefix=""):
        for img_name, img_results in interim.items():
            for result_name, result_id_image in img_results.items():
                interim_result_dir = target_dir / result_name
                if not interim_result_dir.is_dir():
                    interim_result_dir.mkdir(parents=True)
                skimage.io.imsave(target_dir / (file_prefix+result_id_image.name), result_id_image.np_array)

    def empty_dirs(self, *dir_paths):
        for path in dir_paths:
            if path.is_dir():
                shutil.rmtree(path)
            path.mkdir()

    def empty_segmentation_output_dirs(self):
        self.empty_dirs(self.base.output_dir / "2ximages", self.base.output_dir / "cellSizeEstimator", self.base.output_dir / "ensemble",
            self.base.output_dir / "images", self.base.output_dir / "maskrcnn", self.base.output_dir / "presegment", self.base.output_dir / "train_unet",
            self.base.output_dir / "unet_out", self.base.output_dir / "postprocessing")

    def empty_output_dirs(self):
        for path in glob.glob(str(self.base.output_dir / "*")):
            self.empty_dirs(Path(path))

    def prepare_images(self, named_images: Iterable[NamedImage], downscale_factor=1, use_circle_enhancement=True, **kwargs):
        self.empty_dirs(self.base.originals_dir, self.base.pre_preprocessing_script_dir, self.base.post_preprocessing_script_dir)
        """Previously we tried to cut images into tiles, because the segmentation of
        larger images caused an out of memory error. But simply downscaling the
        images will fix this as well and even improves the final segmentation results.
        """
        prepared_images = {}
        for i in range(len(named_images)):
            image = named_images[i]
            image_name = image.name if image.name is not None else f"{i}.tiff"
            try:
                skimage.io.imsave(self.base.originals_dir / image_name, image.np_array)
                image_np_array = image.np_array
                if use_circle_enhancement:
                    image_np_array = self.base.enhance_img_with_circle_detection(image_np_array)
                image_np_array = skimage.transform.resize(image_np_array,
                    (image_np_array.shape[0] // downscale_factor, image_np_array.shape[1] // downscale_factor), anti_aliasing=True)
                image_np_array = skimage.util.img_as_uint(image_np_array)
                skimage.io.imsave(self.base.pre_preprocessing_script_dir / image_name, image_np_array)
                prepared_images[image_name] = NamedImage(image_name, image_np_array)
            except Exception as e:
                print(f"Failure writing image {image_name}. Corrupt image?")
                print(e)
        rc = chcwd(self.base.start_preprocessing, self.base.root_dir)()
        print(rc.stdout.decode("utf-8"))
        for name, named_image in prepared_images.items():
            named_image.name = f"{Path(name).stem}.png"
            image.np_array = skimage.io.imread(self.base.post_preprocessing_script_dir / named_image.name)
        return prepared_images


    def segmentation_full(self, named_images: Iterable[NamedImage]):
        self.empty_segmentation_output_dirs()
        # self.base.cleanup_output()
        for i in range(len(named_images)):
            image = named_images[i]
            image_name = image.name
            try:
                skimage.io.imsave(self.base.post_preprocessing_script_dir / image_name, image.np_array)
            except Exception as e:
                print(f"Failure writing image {image_name}. Corrupt image?")
                print(e)

        rc = chcwd(self.base.start_prediction_full, self.base.root_dir)()
        print(rc.stdout.decode("utf-8"))
        segmented_images = {}
        for i in range(len(named_images)):
            image = named_images[i]
            image_path = self.base.output_dir / "postprocessing" / f"{Path(image.name).stem}.tiff"
            image.np_array = skimage.io.imread(image_path)
            segmented_images[image.name] = image
        return segmented_images


    """
    def segmentation_fast(self, **kwargs):
        self.base.cleanup_output()
        rc = chcwd(self.base.start_prediction_fast, self.base.root_dir)()
        print(rc.stdout.decode("utf-8"))
    """

from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step
from wsi_pipeline.experiments.dl_based_image_cell_segmentation_with_mask_rcnn.adapters import NucleiSegmentationMaskGeneratorAdapter
from pathlib import Path
import wsi_pipeline.os.file as File
from wsi_pipeline.image.id_image import IdImage
from typing import List
import wget
import zipfile
import os
import shutil
from itertools import chain
import skimage.io
import skimage.transform
from wsi_pipeline.util.functions import catch
from wsi_pipeline.image.transform import img_to_tiles
import wsi_pipeline.image.segmentation_utils as segutils
from wsi_pipeline.experiments.shared.files import SharedDatasets

ROOT_DIR = Path(__file__).parent.resolve()
RESULTS_BASE_DIR = ROOT_DIR / "results"
TMP_BASE_DIR = ROOT_DIR / "tmp"
RESULTS_BASE_DIR.mkdir(parents=True, exist_ok=True)
TMP_BASE_DIR.mkdir(parents=True, exist_ok=True)

def retrieve_weights(mask_rcnn_weights_dir):
    File.download_file("https://schmittmann.me/index.php/s/2DubYqV9pzs4ax8/download", Path(f"{mask_rcnn_weights_dir}.zip"))
    catch(File.extract_zip)(Path(f"{mask_rcnn_weights_dir}.zip"), mask_rcnn_weights_dir)

def prepare_tile_ds1(img, scale_factor):
    """Simple image preparation for Mask RCNN model"""
    img = skimage.exposure.equalize_adapthist(img)
    img = skimage.exposure.adjust_log(img)
    img = skimage.transform.rescale(img, (scale_factor, scale_factor, 1))
    img = img * 255.0
    return img

def split_images_ds1(ds1_img_iter, original_tile_size):
    id_images = []
    for img_path in ds1_img_iter:
        base_filename = Path(img_path).stem
        img = skimage.io.imread(img_path) #
        print(img_path)
        # First split the original image into the required tiles. E.g. 1920x1920px tile from above.
        for i, tile in enumerate(img_to_tiles(img, original_tile_size)):
            filename = base_filename+'_tile'+str(i)
            id_images.append(IdImage(f"ds1_{filename}", tile))
    return id_images

def prepare_images_exp1(ds1_img_iter, original_tile_size, scale_factor):
    """ The pretrained Mask RCNN model seens to works best with tiles of size 320x320px and a magnification of 10 (tested empirically).
    So we need to rescale all images of other magnifications and split them into tiles.
    E.g. to transform an image of size 2560x1920px with magnification of 60x into a tile of size 320x320px with magnification 10x,
    we would need to extract a tile of size 1920x1920px from the original image and downscale it with a factor of 10/60.
    """
    id_images = split_images_ds1(ds1_img_iter, original_tile_size)

    for id_tile in id_images:
        # Here we rescale our image to target size and do some more image preparation to improve segmentation
        id_tile.data = prepare_tile_ds1(id_tile.data, scale_factor)

    return id_images

def postprocess_cell_mask(mask, scale_factor):
    # Because we want to apply the segmentation results to our original image, 
    # we need to scale back the results from our model 
    #rescaled_rois = r['rois'] * 1/scale_factor

    mask = skimage.util.img_as_bool(mask)
    mask = skimage.transform.rescale(mask, 1/scale_factor)
    return mask
"""
def prepare_images_ds2(data):
    ds2_img_iter = chain(Path(data["ds2"]["tmp_dir"] / "m3").glob("*.tif"),
                        Path(data["ds2"]["tmp_dir"] / "other").glob("*.tif"))
    id_images = []
    for img_path in ds2_img_iter:
        im = skimage.io.imread(img_path)
        im = skimage.transform.resize(im, (im.shape[0] // 2, im.shape[1] // 2), anti_aliasing=True)
        id_images.append(IdImage(f"ds2_{img_path.name}", im))
    return id_images
"""
def pipeline_experiment_1():
    adapter = NucleiSegmentationMaskGeneratorAdapter()
    datasets = SharedDatasets()

    mask_rcnn_weights_dir = TMP_BASE_DIR / "exp1_mask_rcnn_weights"

    results_dir = RESULTS_BASE_DIR / "experiment_1"
    results_dir.mkdir(parents=True, exist_ok=True)

    ds1_img_iter = Path(datasets.data_base_dir / "ds1" / "60").glob("*.jpg")
    magnification = 60
    target_magnification = 10

    retrieve_weights(mask_rcnn_weights_dir)
    adapter.setup_model(mask_rcnn_weights_dir / "mask_rcnn_weights.h5")
    
    datasets.retrieve_datasets()

    target_tile_size = int(32 * target_magnification) 
    scale_factor = target_magnification / magnification
    original_tile_size = int(target_tile_size / scale_factor)

    id_images = prepare_images_exp1(ds1_img_iter, original_tile_size, scale_factor)
    
    for id_image in id_images:
        skimage.io.imsave(results_dir / Path(f"{id_image.id}_cell_{i:04d}.png"), mask)

        results = adapter.detect([id_image], verbose=1)
        r = results[0]
        for i in range(r["masks"].shape[2]):
            mask = r["masks"][:,:,i]
            mask = postprocess_cell_mask(mask, scale_factor)            
            mask = skimage.util.img_as_uint(mask)
            skimage.io.imsave(results_dir / Path(f"{id_image.id}_cell_{i:04d}.png"), mask)
        break

if __name__ == "__main__":
    # execute only if run as a script
    pipeline_experiment_1()
    #pipeline = pipeline_experiment_1()

    #pipeline()




def segment_ds1():
    pass
    """The Mask RCNN model returns two results for each segmented cell, which are useful for our purpose:
    1) Bounding boxes around the segmented cells called region of interest (r['rois']) in the shape [xmin, ymin, xmax, ymax]. 
        r['rois'].shape == (nr_of_cells, 4)
    2) Binary masks (r['masks']) in the shape of the rescaled image. The last dimension of the nparray is for the segmented cells
        =>r['masks'].shape == (320, 320, nr_of_cells), with target_tile_size = 320 
    """
    """
    # Because we want to apply the segmentation results to our original image, 
    # we need to scale back the results from our model 
    rescaled_rois = r['rois'] * 1/scale_factor
    rescaled_masks = skimage.transform.rescale(r['masks'], (1/scale_factor, 1/scale_factor, 1))
    
    _, ax = plt.subplots(1, figsize=(16,16))
    # Helper function from original Matterport repository to display Mask RCNN results
    # Visualize (and save) colored cell outline/boundaries inside original tile  
    visualize.display_instances(original_tile, rescaled_rois, rescaled_masks,r['class_ids'],['BG', 'Nuclei'],
                    ax=ax, show_mask=False, show_bbox=False,
                    title="Predictions")  
    self.save_figure_results("tile_cells_outline", magnification, filename)
    
    # Visualize (and save) colored cell overlay/mask inside original tile  
    _, ax = plt.subplots(1, figsize=(16,16))
    visualize.display_instances(original_tile, rescaled_rois, rescaled_masks,r['class_ids'],['BG', 'Nuclei'],
                    ax=ax, show_mask=True, show_bbox=False,
                    title="Predictions") 
    self.save_figure_results("tile_cells_masked_dir", magnification, filename)
        
    # Now we move away from visualizing the complete tile and instead display individual cells 
    # First we transform the roi results into individual masks, which are still the size of the original tile   
    roi_masks = np.array(self.rois_to_masks(rescaled_rois, original_tile))
    roi_masks = np.moveaxis(roi_masks, 0, -1) # ((roi_masks.shape[2], roi_masks.shape[1], roi_masks.shape[0]))
    
    # Next we apply the individual cell masks to the original tile and save only the resulting segmented cells.
    # Here we save the individual cells as rectangular extraction the size of their roi from the original tile
    self.save_cell_masks(filename, magnification, 'cell_rois', original_tile, roi_masks)
    # Here we save the individual cells as extraction still the size of their roi from the orignal tile,
    # but now every pixel not segmented as cell is set to (0,0,0)
    self.save_cell_masks(filename, magnification, 'cell_masks', original_tile, rescaled_masks)
    
    # Because the model is pretrained on nuclei, the results usually do not contain the whole cell. 
    # Therefore the next step is extending the roi and repeating the above extraction from the original tile. 
    extended_rois = self.get_extended_cell_rois(r, original_tile, rescaled_rois)
    rescaled_cell_roi_masks = self.rois_to_masks(extended_rois, original_tile)
    
    # Visualize (and save) rectangular extraction the size of their extended roi from the original tile
    _, ax = plt.subplots(1, figsize=(16,16))
    visualize.display_instances(original_tile, extended_rois, np.zeros(r['masks'].shape),r['class_ids'],['BG', 'Nuclei'],
                    ax=ax, show_mask=False, show_bbox=True,
                    title="Predictions")  
    self.save_figure_results("tile_cells_extended_boxes", magnification, filename)
    
    # When we extend the roi of each cell, we naturally start to create a stronger overlap between cells.
    # To prevent multiple cells in the same picture, for every cell we set all pixels belonging to other cells to 
    # the average background color of all non-cell pixels (cell_rois_extended_wo_neighbors)
    rescaled_cell_roi_masks_union = self.unify_cell_roi_masks(original_tile, rescaled_cell_roi_masks)
    background_color = self.get_avg_background_color(original_tile, rescaled_cell_roi_masks_union)
    cell_rois_extended, cell_rois_extended_wo_neighbors = self.get_segmented_cells_rois(original_tile, rescaled_masks, rescaled_cell_roi_masks, rescaled_cell_roi_masks_union, background_color)
    # Here we save the individual cells as rectangular extraction the size of their extended roi from the orignal tile
    self.save_segmented_cells(filename, magnification, 'cell_rois_extended', cell_rois_extended)
    # Here we save the individual cells the same as "cell_rois_extended", but with neighbor cells replaced by 
    # the average background color of all non-cell pixels (cell_rois_extended_wo_neighbors)
    self.save_segmented_cells(filename, magnification, 'cell_rois_extended_wo_neighbors', cell_rois_extended_wo_neighbors)
    """
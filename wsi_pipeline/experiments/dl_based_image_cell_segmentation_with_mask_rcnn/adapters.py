from wsi_pipeline.libs.dl_based_image_cell_segmentation_with_mask_rcnn.nuclei_segmentation_mask_generator import NucleiSegmentationMaskGenerator
import wsi_pipeline.os.file as File
from wsi_pipeline.os.cwd import chcwd
from wsi_pipeline.image.id_image import IdImage
from wsi_pipeline.util.functions import catch
import skimage.io
import skimage.transform
import skimage.util
from typing import List
import os
import shutil
from wsi_pipeline.os.cwd import chcwd
import time

class NucleiSegmentationMaskGeneratorAdapter(object):

    def __init__(self):
        self.base = NucleiSegmentationMaskGenerator() 

    def setup_model(self, mask_rcnn_weights):
        self.base.setup_model(mask_rcnn_weights)
        #self.base.cleanup_weights()
        #shutil.copyfile(mask_rcnn_weights, self.base.weights_dir / mask_rcnn_weights.name)

    def detect(self, id_images, verbose=1):
        return self.base.detect([id_image.data for id_image in id_images], verbose=1)
        
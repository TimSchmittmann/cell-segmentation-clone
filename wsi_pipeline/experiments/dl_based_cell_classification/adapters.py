from wsi_pipeline.libs.biomagdsb_cell_segmentation.style_transfer_segmentation import StyleTransferSegmentation
import wsi_pipeline.os.file as File
from wsi_pipeline.os.cwd import chcwd
from wsi_pipeline.image.id_image import IdImage
from wsi_pipeline.util.functions import catch
import skimage.io
import skimage.transform
import skimage.util
from typing import List
import os
import shutil

class CellTypeClassifierAdapter(object):

    def __init__(self):
        self.base = CellTypeClassifier() 
        
    def predict(self, image, **kwargs):        
        self.base.predict_image(image)
        
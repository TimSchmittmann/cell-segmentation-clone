import wsi_pipeline.os.file as File
from pathlib import Path
from wsi_pipeline.util.functions import catch

ROOT_DIR = Path(__file__).parent.resolve()
TMP_BASE_DIR = ROOT_DIR / "tmp"
TMP_BASE_DIR.mkdir(parents=True, exist_ok=True)



class M3NonM3RemoteDataset(object):

    def __init__(self):
        self.data = { "ds1": { "url":"https://schmittmann.me/index.php/s/or393w0KtB8EQjV/download"},
                "ds2": {"url": "https://schmittmann.me/index.php/s/7YOCYTHIROv4C9X/download"}}
        self.data = self.set_data_tmp_dirs(self.data, TMP_BASE_DIR)
        self.data_base_dir = ROOT_DIR / "data"
        
    def set_data_tmp_dirs(self, data, tmp_base_dir):
        for key, ds in data.items():
            ds["tmp_file"] = tmp_base_dir / f"{key}_tmp.zip"
        return data

    def retrieve_images(self, data, data_base_dir):
        for key, ds in data.items():
            tmp_file = ds["tmp_file"]
            File.download_file(ds["url"], tmp_file)
            catch(File.extract_zip)(tmp_file, data_base_dir / key)
        return data

    def retrieve_datasets(self):
        self.retrieve_images(self.data, self.data_base_dir)
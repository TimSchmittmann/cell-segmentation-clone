import wsi_pipeline.os.file as File
from wsi_pipeline.os.cwd import chcwd
from wsi_pipeline.image.id_image import IdImage
from wsi_pipeline.util.functions import catch
import skimage.io
import skimage.transform
import skimage.util
from typing import List
import os
import shutil

class RegionpropsExtractorAdapter(object):
    def __init__(self):
        self.base = RegionpropsExtractor() 


class SegmentationMaskTransformerAdapter(object):
    def __init__(self):
        self.base = SegmentationMaskTransformer() 
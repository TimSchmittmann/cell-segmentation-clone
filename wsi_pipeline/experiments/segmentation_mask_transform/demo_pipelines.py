from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step
from wsi_pipeline.experiments.shared import SharedDatasets, TMP_BASE_DIR
from pathlib import Path
import wsi_pipeline.os.file as File
from wsi_pipeline.image.id_image import IdImage
from typing import List
import wget
import zipfile
import os
import shutil
from itertools import chain
import skimage.io
import skimage.transform
from wsi_pipeline.util.functions import catch
from PIL import Image

ROOT_DIR = os.path.abspath("")
DATA_DIR = os.path.join(ROOT_DIR, 'data')
RESULTS_DIR = os.path.join(ROOT_DIR, 'results')
RUN_DIR = os.path.join(ROOT_DIR, 'run')

def pipeline_experiment_1():
    adapter = RegionpropsAdapter()
    datasets = SharedDatasets()

    id_images = prepare_images_ds2(datasets.data)
    adapter.prepare_images(id_images)

if __name__ == "__main__":
    # execute only if run as a script
    pipeline_experiment_1()
    #pipeline = pipeline_experiment_1()

    #pipeline()

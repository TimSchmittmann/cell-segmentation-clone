import skimage

class FsImgReader(object):

  def __init__(self, file_paths):
    self.file_paths = file_paths

  def __iter__(self):
    return self

  def __next__(self):
    for path in self.file_paths:
      yield skimage.io.imread(path)
    raise StopIteration
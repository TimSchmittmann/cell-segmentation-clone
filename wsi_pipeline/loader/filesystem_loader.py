"""
from wsi_pipeline.loader.data_loader import DataLoader
from wsi_pipeline.core.step import Step
from pathlib import Path
import wget
import zipfile
import os
import shutil
class FilesystemLoader(DataLoader):

    @as_step
    def copy_files(self, from_directory, to_directory, pattern="*", force_overwrite=False, **kwargs):
        for path in Path(from_directory).glob(pattern):
            target_path = os.path.join(to_directory, os.path.basename(path))
            if os.path.isfile(target_path) and not force_overwrite:
                continue
            shutil.copy(path, to_directory) 
"""
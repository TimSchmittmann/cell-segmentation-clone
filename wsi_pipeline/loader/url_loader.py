"""
from wsi_pipeline.loader.data_loader import DataLoader
from wsi_pipeline.core.step import Step
from wsi_pipeline.core.step import as_step
import pathlib
import wget
import zipfile
import os

class UrlLoader(DataLoader):

    @as_step
    def extract_zip(self, url, extraction_target_dir, force_overwrite=False, )

    @as_step
    def download_and_extract_zip(tmp_file = "tmp.zip", **kwargs):
        if os.listdir(extraction_target_dir) != [] and not force_overwrite:
            return 
        if not os.path.isdir(extraction_target_dir):
            os.mkdir(extraction_target_dir)
        wget.download(url, tmp_file)
        with zipfile.ZipFile(tmp_file, 'r') as zip_ref:
            zip_ref.extractall(extraction_target_dir)
        pathlib.Path.unlink(tmp_file)


    @as_step
    def download_file(self, url, filename, **kwargs):
        wget.download(url, filename)

"""
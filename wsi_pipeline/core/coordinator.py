from wsi_pipeline.core.pipeline import Pipeline
from wsi_pipeline.core.step import Step

class Coordinator(object):

    def run(self):
        p = Pipeline("test")
        print(p.name)

if __name__ == "__main__":
    c = Coordinator()
    c.run()

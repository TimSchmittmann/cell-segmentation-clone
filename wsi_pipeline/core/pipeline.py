from wsi_pipeline.core.step import Step
from wsi_pipeline.core.data_step import DataStep
from wsi_pipeline.core.data import Data
import copy

class Pipeline(Step):

    def __init__(self, name, remember_data=False, *steps):
        super().__init__(name, None)
        
        self.data_at_step = []
        self.remember_data = remember_data
        self.steps = steps

    def add(self,step):
        self.steps = (*self.steps, step)

    def __call__(self, **execution_kwargs):
        data = Data()
        for step in self.steps:
            if data and isinstance(step, DataStep): 
                t_data = step(data, **execution_kwargs)
            else:
                t_data = step(**execution_kwargs)
            if t_data is not None:
                data = t_data                
            if self.remember_data:
                self.data_at_step.append(copy.deepcopy(data))

        return data
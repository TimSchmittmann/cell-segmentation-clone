import functools
from wsi_pipeline.core.step import Step
from wsi_pipeline.core.data import Data

class DataStep(Step):

    def __call__(self, data: Data = Data(), **execution_kwargs) -> Data:
        kwargs = {**self.kwargs, **execution_kwargs}
        data = self.func(*self.args, data=data,  **kwargs)
        return data

import functools
from pprint import pprint
import inspect

class Step(object):

    def __init__(self, name, func, *args, **kwargs):
        self.name = name if name is not None else func.__name__
        self.func = func
        self.args = args
        self.kwargs = kwargs

    def __call__(self, **execution_kwargs):
        kwargs = {**self.kwargs, **execution_kwargs}
        self.func(*self.args, **kwargs)

    @classmethod
    def make(cls, _func=None, *, name=None):
        def decorator_step(func):
            @functools.wraps(func)
            def wrapper_step(*args, **kwargs):
                return cls(name, func, *args, **kwargs)
            return wrapper_step

        if _func is None:
            return decorator_step
        else:
            return decorator_step(_func)

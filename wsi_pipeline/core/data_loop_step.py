from wsi_pipeline.core.data_step import DataStep
from wsi_pipeline.core.data import Data

class DataLoopStep(DataStep):

    def __call__(self, data: Data = Data(), **execution_kwargs) -> Data:
        kwargs = {**self.kwargs, **execution_kwargs}
        for i in range(len(data)): 
            data[i] = self.func(*self.args, data[i], **kwargs)
        return data
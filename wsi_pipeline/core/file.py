import os
import shutil
import wget
import zipfile
from pathlib import Path
from wsi_pipeline.core.data_loop_step import DataLoopStep
from wsi_pipeline.core.data import Data
#from wsi_pipeline.core.step import Step

@DataLoopStep.make
def copy_file(to_directory, force_overwrite=False, data: Data = Data()) -> str:
    file_path = data
    print(f"Copy {file_path} to {to_directory}")
    target_path = os.path.join(to_directory, os.path.basename(file_path))
    if os.path.isfile(target_path) and not force_overwrite:
        return file_path
    shutil.copy(file_path, to_directory) 
    return target_path

@DataLoopStep.make
def move_file(to_directory, force_overwrite=False, data: Data = Data()):
    file_path = data
    target_path = os.path.join(to_directory, os.path.basename(file_path))
    if os.path.isfile(target_path) and not force_overwrite:
        return file_path
    shutil.move(file_path, to_directory) 
    return target_path

@DataLoopStep.make
def glob_paths(from_dir: Data = Data(), pattern="*") -> Data:
    print(f"Glob {from_dir}")
    return Path(from_dir).glob(pattern)
"""
@as_step
def copy_files(self, from_directory, to_directory, pattern="*", force_overwrite=False, **kwargs):
    for path in Path(from_directory).glob(pattern):
        target_path = os.path.join(to_directory, os.path.basename(path))
        if os.path.isfile(target_path) and not force_overwrite:
            continue
        shutil.copy(path, to_directory) 
"""
@DataLoopStep.make
def extract_zip(extraction_target_dir, force_overwrite=False, data: Data = Data()) -> Data:
    zip_file = data
    print(f"Unzip {zip_file}")
    with zipfile.ZipFile(zip_file, 'r') as zip_ref:
        zip_ref.extractall(extraction_target_dir)
    return extraction_target_dir
"""
@as_step
def download_and_extract_zip(tmp_file = "tmp.zip", **kwargs):
    if os.listdir(extraction_target_dir) != [] and not force_overwrite:
        return 
    if not os.path.isdir(extraction_target_dir):
        os.mkdir(extraction_target_dir)
    wget.download(url, tmp_file)

    pathlib.Path.unlink(tmp_file)
"""
@DataLoopStep.make
def mkdir_if_not_exist(data: Data = Data()) -> str:
    path = data
    if not os.path.isdir(path):
        os.mkdir(path)
    return


@DataLoopStep.make
def download_file(filename, data: Data = Data()) -> str:
    url = data
    print(f"Download {url}")
    wget.download(url, filename)
    return filename

@DataLoopStep.make
def delete_file(data: Data = Data()) -> None:
    file_path = data
    print(f"Delete {file_path}")
    Path.unlink(file_path)

